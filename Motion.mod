<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
	<UiMod name="Motion" version="1.2.6-rormod" date="2017/07">
		<Author name="Wikki" email="wikkifizzle@gmail.com" />
		<Description text="Motion - Making apothecaries and talisman makers happy since 1847! ROR mod by anon" />
		<VersionSettings gameVersion="1.4.0" windowsVersion="1.0" savedVariablesVersion="2.0" />
		<Dependencies>
			<Dependency name="EA_BackpackWindow" />
			<Dependency name="EA_CraftingSystem" />
			<Dependency name="EASystem_Strings" />
			<Dependency name="EASystem_Utils" />
			<Dependency name="EASystem_ResourceFrames" />
			<Dependency name="EASystem_WindowUtils" />
            <Dependency name="EASystem_Tooltips" />
            <Dependency name="EATemplate_DefaultWindowSkin" />
            <Dependency name="LibSlash" />
        </Dependencies>
        
        <SavedVariables>
			<SavedVariable name="Motion.Profiles" global="true" />
		</SavedVariables>
		
		<Files>
			<File name="Assets/Assets.xml" />
		
			<File name="Libraries/LibStub.lua" />
			<File name="Libraries/LibGUI.lua" />
			<File name="Libraries/AceLocale-3.0.lua" />
			
			<File name="Localization/deDE.lua" />
			<File name="Localization/enUS.lua" />
			<File name="Localization/esES.lua" />
			<File name="Localization/frFR.lua" />
			<File name="Localization/itIT.lua" />
			<File name="Localization/jaJP.lua" />
			<File name="Localization/koKR.lua" />
			<File name="Localization/ruRU.lua" />
			<File name="Localization/zhCN.lua" />
			<File name="Localization/zhTW.lua" />
			
			<!-- Start Configuration Windows -->
			<File name="Configuration/Config.xml" />
			<File name="Configuration/Config.lua" />
			<File name="Configuration/Config_General.lua" />
			<File name="Configuration/Config_Profiles.lua" />
			<!-- <File name="Configuration/Config_About.lua" /> -->
			
			<File name="Source/Motion.lua" />
			<File name="Source/Motion.xml" />
			<File name="Source/CraftInterface.lua" />
			
			<File name="Source/Profiles.lua" />
		</Files>

		<OnUpdate>
			<CallFunction name="Motion.OnUpdate"/>
		</OnUpdate>
		<OnInitialize>
			<CreateWindow name="Motion_Tooltip_TwoLine" show="false" />
			<CreateWindow name="MotionConfig" show="false" />
			<CallFunction name="Motion.OnInitialize" />
		</OnInitialize>
		<OnShutdown/>
		
		<WARInfo>
		    <Categories>
		        <Category name="ITEMS_INVENTORY" />
		        <Category name="CRAFTING" />
		    </Categories>
		    <Careers>
		        <Career name="BLACKGUARD" />
		        <Career name="WITCH_ELF" />
		        <Career name="DISCIPLE" />
		        <Career name="SORCERER" />
		        <Career name="IRON_BREAKER" />
		        <Career name="SLAYER" />
		        <Career name="RUNE_PRIEST" />
		        <Career name="ENGINEER" />
		        <Career name="BLACK_ORC" />
		        <Career name="CHOPPA" />
		        <Career name="SHAMAN" />
		        <Career name="SQUIG_HERDER" />
		        <Career name="WITCH_HUNTER" />
		        <Career name="KNIGHT" />
		        <Career name="BRIGHT_WIZARD" />
		        <Career name="WARRIOR_PRIEST" />
		        <Career name="CHOSEN" />
		        <Career name= "MARAUDER" />
		        <Career name="ZEALOT" />
		        <Career name="MAGUS" />
		        <Career name="SWORDMASTER" />
		        <Career name="SHADOW_WARRIOR" />
		        <Career name="WHITE_LION" />
		        <Career name="ARCHMAGE" />
		    </Careers>
		</WARInfo>

	</UiMod>
</ModuleFile>