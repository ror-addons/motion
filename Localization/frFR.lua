--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Motion/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Motion", "frFR")
if not T then return end

-- T["(Motion) use /motion to bring up configuration window."] = L""
T["About Motion"] = L"� propos de Motion"
T["Active Profile:"] = L"Profil actif :"
T["Apply"] = L"Appliquer"
-- T["Are you sure?"] = L""
T["Clear Ingredients"] = L"Effacer ingr�dients"
T["Close"] = L"Fermer"
T["Configuration"] = L"Configuration"
T["Copy Settings From:"] = L"Copier les r�glages depuis :"
-- T["Create"] = L""
-- T["Create New Profile"] = L""
-- T["Create New Profile:"] = L""
T["Current Crafting Status"] = L"Status actuel de fabrication"
T["Current Profile:"] = L"Profil actuel :"
T["Default Makes:"] = L"Exemplaires par d�faut :"
-- T["Delete"] = L""
T["Do you wish to delete the following recipe?"] = L"Souhaitez-vous supprimer la recette suivante ?"
T["Enable Debugging"] = L"Activer le d�bogage"
T["Enforce Optimal Resource Counts"] = L"Forcer l'optimisation par compteur de ressources"
T["General"] = L"G�n�ral"
-- T["L-Click: List the saved recipes"] = L""
T["L-Click: Save the current recipe"] = L"Clic G : Sauvegarder la recette actuelle"
T["Motion %s initialized."] = L"Motion %s initialis�."
T["Motion - Configuration"] = L"Motion - Configuration"
T["Motion - Save Recipe"] = L"Motion - Sauvegarde de recette"
-- T["Motion Toggle"] = L""
T["NOTE: Profile changes take effect immediately!"] = L"NOTE : Les changements de profil prennent effet imm�diatement !"
T["New Recipe"] = L"Nouvelle recette"
T["No"] = L"Non"
T["No items found."] = L"Aucun objet trouv�."
T["No recipes found."] = L"Aucune recette trouv�e."
T["Please enter a name for the new recipe."] = L"Merci de saisir un nom pour la nouvelle recette."
T["Profiles"] = L"Profils"
T["R-Click: List the saved recipes"] = L"Clic D : Lister les recettes sauvegard�es"
-- T["R-Click: Save the current recipe"] = L""
T["Reset Profile"] = L"R�initialiser le profil"
T["Revert"] = L"R�tablir"
T["Right click to display menu."] = L"Clic droit pour afficher le menu."
T["Saved Recipe(s)"] = L"Recette(s) sauvegard�e(s)"
T["Set Active Profile:"] = L"Choix du profil actif :"
-- T["Show/Hide Motion"] = L""
T["Start Making Item(s)"] = L"Commencer la fabrication d'objet(s)"
T["Stop Crafting On Combat"] = L"Arr�t de la production en entrant en combat"
T["Stop Crafting On Spellcast"] = L"Arr�t de la production en lan�ant un sort"
T["Stop Making Item(s)"] = L"Arr�ter la fabrication d'objet(s)"
T["This is the default number of makes Motion will make."] = L"Ceci est le nombre d'exemplaires que Motion produira par d�faut."
T["Toggle Configuration"] = L"Panneau de configuration"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"ATTENTION : Cliquer sur ce bouton restaurera les valeurs par d�faut pour le profil actuel !"
T["With this enabled Motion will output debugging information to the Debug Log.  This information can be provided to the developer to help diagnose problems."] = L"En activant cette option, Motion retournera des informations au Journal de D�bogage. Ces informations sont destin�es aux d�veloppeurs afin d'aider � diagnostiquer les probl�mes."
T["With this option enabled you will stop crafting if you cast a spell."] = L"En activant cette option, la r�alisation d'objet sera interrompue si vous lancez un sort."
T["With this option enabled you will stop crafting if you go into combat."] = L"En activant cette option le processus de fabrication sera interrompu si vous entrez en combat."
T["With this setting enabled, Motion will enforce a minimum amount of resources to be present before it will create an item.  This helps to prevent making items that are not to their full potenital."] = L"Si cette option est activ�e, Motion requiert de disposer d'une quantit� minimum de ressources n�cessaires avant de lancer la cr�ation d'objet. Ceci permet d'emp�cher que des objets ne soient pas fabriqu�s � leur plein potentiel en raison de ressources manquantes."
T["Yes"] = L"Oui"

