--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Motion/localization/
--

local debug = false
--[===[@debug@
debug = true
--@end-debug@]===]

local T = LibStub( "WAR-AceLocale-3.0" ):NewLocale( "Motion", "enUS", true, debug )

T["(Motion) use /motion to bring up configuration window."] = L"(Motion) use /motion to bring up configuration window."
T["About Motion"] = L"About Motion"
T["Active Profile:"] = L"Active Profile:"
T["Apply"] = L"Apply"
T["Are you sure?"] = L"Are you sure?"
T["Clear Ingredients"] = L"Clear Ingredients"
T["Close"] = L"Close"
T["Configuration"] = L"Configuration"
T["Copy Settings From:"] = L"Copy Settings From:"
T["Create"] = L"Create"
T["Create New Profile"] = L"Create New Profile"
T["Create New Profile:"] = L"Create New Profile:"
T["Current Crafting Status"] = L"Current Crafting Status"
T["Current Profile:"] = L"Current Profile:"
T["Default Makes:"] = L"Default Makes:"
T["Delete"] = L"Delete"
T["Do you wish to delete the following recipe?"] = L"Do you wish to delete the following recipe?"
T["Enable Debugging"] = L"Enable Debugging"
T["Enforce Optimal Resource Counts"] = L"Enforce Optimal Resource Counts"
T["General"] = L"General"
T["L-Click: List the saved recipes"] = L"L-Click: List the saved recipes"
T["L-Click: Save the current recipe"] = L"L-Click: Save the current recipe"
T["Motion %s initialized."] = L"Motion %s initialized."
T["Motion - Configuration"] = L"Motion - Configuration"
T["Motion - Save Recipe"] = L"Motion - Save Recipe"
T["Motion Toggle"] = L"Motion Toggle"
T["NOTE: Profile changes take effect immediately!"] = L"NOTE: Profile changes take effect immediately!"
T["New Recipe"] = L"New Recipe"
T["No"] = L"No"
T["No items found."] = L"No items found."
T["No recipes found."] = L"No recipes found."
T["Please enter a name for the new recipe."] = L"Please enter a name for the new recipe."
T["Profiles"] = L"Profiles"
T["R-Click: List the saved recipes"] = L"R-Click: List the saved recipes"
T["R-Click: Save the current recipe"] = L"R-Click: Save the current recipe"
T["Reset Profile"] = L"Reset Profile"
T["Revert"] = L"Revert"
T["Right click to display menu."] = L"Right click to display menu."
T["Saved Recipe(s)"] = L"Saved Recipe(s)"
T["Set Active Profile:"] = L"Set Active Profile:"
T["Show/Hide Motion"] = L"Show/Hide Motion"
T["Start Making Item(s)"] = L"Start Making Item(s)"
T["Stop Crafting On Combat"] = L"Stop Crafting On Combat"
T["Stop Crafting On Spellcast"] = L"Stop Crafting On Spellcast"
T["Stop Making Item(s)"] = L"Stop Making Item(s)"
T["This is the default number of makes Motion will make."] = L"This is the default number of makes Motion will make."
T["Toggle Configuration"] = L"Toggle Configuration"
T["WARNING: Clicking this button will restore the current profile back to default values!"] = L"WARNING: Clicking this button will restore the current profile back to default values!"
T["With this enabled Motion will output debugging information to the Debug Log.  This information can be provided to the developer to help diagnose problems."] = L"With this enabled Motion will output debugging information to the Debug Log.  This information can be provided to the developer to help diagnose problems."
T["With this option enabled you will stop crafting if you cast a spell."] = L"With this option enabled you will stop crafting if you cast a spell."
T["With this option enabled you will stop crafting if you go into combat."] = L"With this option enabled you will stop crafting if you go into combat."
T["With this setting enabled, Motion will enforce a minimum amount of resources to be present before it will create an item.  This helps to prevent making items that are not to their full potenital."] = L"With this setting enabled, Motion will enforce a minimum amount of resources to be present before it will create an item.  This helps to prevent making items that are not to their full potenital."
T["Yes"] = L"Yes"
