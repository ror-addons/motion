--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

--
-- The localization information present within this file is automatically
-- packaged by the CurseForge packager.
--
-- Incorrect or missing translations can be updated here:
-- http://war.curseforge.com/projects/Motion/localization/
--

local T = LibStub("WAR-AceLocale-3.0"):NewLocale("Motion", "jaJP")
if not T then return end

-- T["(Motion) use /motion to bring up configuration window."] = L""
-- T["About Motion"] = L""
-- T["Active Profile:"] = L""
-- T["Apply"] = L""
-- T["Are you sure?"] = L""
-- T["Clear Ingredients"] = L""
-- T["Close"] = L""
-- T["Configuration"] = L""
-- T["Copy Settings From:"] = L""
-- T["Create"] = L""
-- T["Create New Profile"] = L""
-- T["Create New Profile:"] = L""
-- T["Current Crafting Status"] = L""
-- T["Current Profile:"] = L""
-- T["Default Makes:"] = L""
-- T["Delete"] = L""
-- T["Do you wish to delete the following recipe?"] = L""
-- T["Enable Debugging"] = L""
-- T["Enforce Optimal Resource Counts"] = L""
-- T["General"] = L""
-- T["L-Click: List the saved recipes"] = L""
-- T["L-Click: Save the current recipe"] = L""
-- T["Motion %s initialized."] = L""
-- T["Motion - Configuration"] = L""
-- T["Motion - Save Recipe"] = L""
-- T["Motion Toggle"] = L""
-- T["NOTE: Profile changes take effect immediately!"] = L""
-- T["New Recipe"] = L""
-- T["No"] = L""
-- T["No items found."] = L""
-- T["No recipes found."] = L""
-- T["Please enter a name for the new recipe."] = L""
-- T["Profiles"] = L""
-- T["R-Click: List the saved recipes"] = L""
-- T["R-Click: Save the current recipe"] = L""
-- T["Reset Profile"] = L""
-- T["Revert"] = L""
-- T["Right click to display menu."] = L""
-- T["Saved Recipe(s)"] = L""
-- T["Set Active Profile:"] = L""
-- T["Show/Hide Motion"] = L""
-- T["Start Making Item(s)"] = L""
-- T["Stop Crafting On Combat"] = L""
-- T["Stop Crafting On Spellcast"] = L""
-- T["Stop Making Item(s)"] = L""
-- T["This is the default number of makes Motion will make."] = L""
-- T["Toggle Configuration"] = L""
-- T["WARNING: Clicking this button will restore the current profile back to default values!"] = L""
-- T["With this enabled Motion will output debugging information to the Debug Log.  This information can be provided to the developer to help diagnose problems."] = L""
-- T["With this option enabled you will stop crafting if you cast a spell."] = L""
-- T["With this option enabled you will stop crafting if you go into combat."] = L""
-- T["With this setting enabled, Motion will enforce a minimum amount of resources to be present before it will create an item.  This helps to prevent making items that are not to their full potenital."] = L""
-- T["Yes"] = L""

