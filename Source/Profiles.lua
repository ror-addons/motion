--
-- This file adapted from SquaredProfiles.lua, from Aiiane's Squared, to work with this addon
--
local Motion = Motion

-- If it doesn't already exist, make it
if not Motion.Profiles then Motion.Profiles = {active=0,names={},data={},perchar={}} end

-- Make utility functions local for performance
local pairs 			= pairs
local unpack 			= unpack
local tonumber			= tonumber
local tostring 			= tostring
local towstring 		= towstring
local max 				= math.max
local min 				= math.min
local wstring_sub 		= wstring.sub
local wstring_format 	= wstring.format
local tinsert 			= table.insert
local tremove 			= table.remove

-- generic deepcopy
local function deepcopy(object)
    local lookup_table = {}
    local function _copy(object)
        if type(object) ~= "table" then
            return object
        elseif lookup_table[object] then
            return lookup_table[object]
        end
        local new_table = {}
        lookup_table[object] = new_table
        for index, value in pairs(object) do
            new_table[_copy(index)] = _copy(value)
        end
        return new_table
    end
    return _copy(object)
end

-- This function takes a defaults table and a settings, and verifies that all 
-- keys that exist in the defaults exist in the settings.
local function updateSettings(defaults, settings)
	if type(defaults) ~= "table" or type(settings) ~= "table" then return end
	for key, value in pairs( defaults ) do
		if type(value) ~= "table" and value ~= nil then
			-- Check if the key exists in settings
			if settings[key] == nil then
				settings[key] = value
			end
		else
			-- Create the table if it doesnt exist, or if its type is not of table
			if settings[key] == nil or type(settings[key]) ~= "table" then
				settings[key] = {}
			end
			
			updateSettings( value, settings[key] )
		end
	end	
end

local function GetCurrentSlot()
	for k,v in pairs(GameData.Account.CharacterSlot) do
		if v.Name == GameData.Player.name then
			return k
		end
	end
	return nil
end

function Motion.Set(key, value)
	if key then
		Motion.Profiles.data[Motion.Profiles.active][key] = value
	end
end

function Motion.Get( key )
	if key then
		return Motion.Profiles.data[Motion.Profiles.active][key]
	end
	return nil
end

function Motion.ResetActiveProfile()
	if Motion.Profiles and Motion.Profiles.data and Motion.Profiles.data[Motion.Profiles.active] then
		Motion.Profiles.data[Motion.Profiles.active] = deepcopy(Motion.DefaultSettings)
		Motion.OnProfileChanged()
	end
end

-- Initialize the profile system (called from Motion.OnLoad)
function Motion.InitProfiles()
    if Motion.Profiles.active == 0 then
		-- Empty profiles table - initialize the 'Default' profile
		Motion.Profiles.active = Motion.AddProfile(L"Default")
		local slot = GetCurrentSlot()
		if not slot then d("Couldn't figure out what character slot this is!") return end
		Motion.Profiles.perchar[slot] = L"Default"
	else
		-- Non-empty profiles table - check for smart activation
		local slot = GetCurrentSlot()
		local profile = Motion.Profiles.perchar[slot]
		if profile then
			Motion.ActivateProfile(profile)
		else 
			Motion.Profiles.perchar[slot] = L"Default"
			Motion.ActivateProfile(L"Default")
		end
	end
end

function Motion.AddProfile(name, source)
	local sourceId = nil
	
	-- First check to see if we're adding a profile which already exists,
	-- if we are then just return that profile's id
	for k,v in ipairs(Motion.Profiles.names) do
		if v == name then return k end
		-- Might as well find the source Id while we're at it
		if v == source then sourceId = k end
	end
	
	-- If it doesn't exist, add it
	tinsert(Motion.Profiles.names, name)
	local data = Motion.GetProfileData(sourceId)
	if data then
		tinsert(Motion.Profiles.data, data)
	else
		tinsert(Motion.Profiles.data, deepcopy(Motion.DefaultSettings))
	end
	
	return #Motion.Profiles.names
end

function Motion.GetProfileData(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(Motion.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return deepcopy(Motion.Profiles.data[source])
	else
		-- invalid profile
		return nil
	end
end

function Motion.GetProfileDataForUse(source)
	-- If specified by name, look up the id
	if type(source) == "wstring" then
		for k,v in ipairs(Motion.Profiles.names) do
			if v == source then
				source = k
				break
			end
		end
	end
	
	if type(source) == "number" then
		return Motion.Profiles.data[source]
	else
		-- invalid profile
		return nil
	end
end

function Motion.ActivateProfile(source, force)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(Motion.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	local sourceName = Motion.Profiles.names[source]
	if not force and Motion.Profiles.active == source then return end
	
	local data = Motion.GetProfileData(source)
	if not data then return end
	Motion.Profiles.active = source
	
	-- Make sure the profile is up to date
	updateSettings( Motion.DefaultSettings, Motion.Profiles.data[Motion.Profiles.active] )
	
	Motion.OnProfileChanged()
	
	local slot = GetCurrentSlot()
	if slot then
		Motion.Profiles.perchar[slot] = sourceName
	end

	return source
end

function Motion.CopyProfileToActive( source )
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(Motion.Profiles.names) do
			if v == source then source = k end
		end
	end
	if type(source) ~= "number" then return end
	
	if Motion.Profiles and Motion.Profiles.data and Motion.Profiles.data[source] and Motion.Profiles.data[Motion.Profiles.active] then
		Motion.Profiles.data[Motion.Profiles.active] = deepcopy(Motion.Profiles.data[source])
		
		updateSettings( Motion.DefaultSettings, Motion.Profiles.data[Motion.Profiles.active] )
		
		Motion.OnProfileChanged()
	end
end

function Motion.GetProfileList()
	return deepcopy(Motion.Profiles.names)
end

function Motion.GetCurrentProfile()
	return Motion.Profiles.active, Motion.Profiles.names[Motion.Profiles.active]
end

function Motion.DeleteProfile(source)
	-- Find the profile if specified by name
	if type(source) == "wstring" then
		for k,v in ipairs(Motion.Profiles.names) do
			if v == source then source = k end
		end
	end
	
	if type(source) ~= "number" then return end
	
	-- Don't allow people to delete the default profile, or a nonexistant one
	if source < 2 or not Motion.Profiles.names[source] then return end
	
	local sourceName = Motion.Profiles.names[source]
	
	-- Remove the profile
	tremove(Motion.Profiles.names, source)
	tremove(Motion.Profiles.data, source)
	
	-- Swap any perchars which were the deleted profile back to default
	local slot = GetCurrentSlot()
	for k,v in pairs(Motion.Profiles.perchar) do
		if v == sourceName then
			Motion.Profiles.perchar[k] = L"Default"
			-- If our current character was set to the deleted profile, activate default
			if k == slot then
				Motion.ActivateProfile(L"Default")
			end
		end
	end
end