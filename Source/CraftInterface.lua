--[[
	This application is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    The applications is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the applications.  If not, see <http://www.gnu.org/licenses/>.
--]]

local Motion = _G.Motion

if Motion.CraftInterface == nil then Motion.CraftInterface = {} end

local new, del, wipe = Motion.new, Motion.del, Motion.wipe

Motion.CraftInterface.TYPE_CONTAINER 	= 1
Motion.CraftInterface.TYPE_DETERMINANT 	= 2
Motion.CraftInterface.TYPE_RESOURCE 	= 3

local Interface = Motion.CraftInterface 

function Interface:Create( type, button, id )
	local this = {}
	setmetatable( this, self )
	self.__index = self
	
	-- Initialize our members
	this.type 		= type
	this.button 	= button
	this.id			= id
	this.itemData	= nil
	
	this.slot			= nil
	this.backpackType	= nil
	
	return this
end

function Interface:AddCraftingItem()
	-- Attempt to retrieve the first location in our inventory with the item 
	if( self.itemData ~= nil ) then
		-- Generate our allocated slots
		local craftingSlots = GetCraftingBackPackSlots( Motion.GetPlayerCraftType() )
		local mSlots = new()
		for k, v in pairs( craftingSlots )
		do
			if( mSlots[v.backpack] == nil ) then mSlots[v.backpack] = {} end
			if( mSlots[v.backpack][v.slot] == nil ) then mSlots[v.backpack][v.slot] = 0 end
			mSlots[v.backpack][v.slot] = mSlots[v.backpack][v.slot] + 1
		end
	
		self.slot, self.backpackType = Motion.RetrieveFirstAvailableItemSlot( self.itemData.uniqueID, mSlots )
		if( self.slot ~= nil and self.backpackType ~= nil ) then
			local playerCraftType = Motion.GetPlayerCraftType()
			if( self.type == Motion.CraftInterface.TYPE_CONTAINER ) then
				Motion.Debug( "Interface:AddCraftingItem - Container" )
				AddCraftingContainer( playerCraftType, self.slot, self.backpackType )
			elseif( self.type == Motion.CraftInterface.TYPE_DETERMINANT or
					self.type == Motion.CraftInterface.TYPE_RESOURCE ) then
				Motion.Debug( "Interface:AddCraftingItem - Item" )
				AddCraftingItem( playerCraftType, self.id, self.slot, self.backpackType )
			end
		end
		
		del(mSlots)
	end
end

function Interface:ClearItemData()
	self:SetItemData( nil )
end

function Interface:GetItemCount()
	local count	= 0
	
	if( self.itemData ~= nil ) then
		for _, set in ipairs( Motion.backpackTypes )
		do
			local data = set.data()
			for slot, item in ipairs( data )
			do
				if( self.itemData.uniqueID == item.uniqueID ) then
					count = count + item.stackCount
				end
			end
		end
	end
	
	return count
end

function Interface:GetItemData()
	return self.itemData
end

function Interface:SetItemData( itemData, slot, type, removeFlag )
	local texture, dx, dy = "", 0, 0
	local color	= { r=222, g=192, b=50 }

	-- Remove our crafting item
	self:RemoveCraftingItem()	
	
	-- Update the item data before updating our display information
	self.itemData = itemData
	
	-- If we have a new item, retrieve its texture, and add it as a crafting item
	if( itemData ~= nil ) then
		texture, textureDx, textureDy = GetIconData( itemData.iconNum )
		color = DataUtils.GetItemRarityColor( itemData )
	end
	
	-- Add our crafting item
	self:AddCraftingItem()
	
	-- Update the item count
	self:UpdateItemCount()
	
	-- Update the icon display
	DynamicImageSetTexture( self.button.name .. "Icon", texture, dx, dy )
	WindowSetTintColor( self.button.overlay.name, color.r, color.g, color.b)
end

function Interface:SetItemDataExplicit( itemData, slot, type )
	local texture, dx, dy = "", 0, 0
	local color	= { r=222, g=192, b=50 }

	-- Update the item data before updating our display information
	self.itemData = itemData
	
	-- If we have a new item, retrieve its texture, and add it as a crafting item
	if( itemData ~= nil ) then
		texture, textureDx, textureDy = GetIconData( itemData.iconNum )
		color = DataUtils.GetItemRarityColor( itemData )
	end

	-- Update the 	
	self.slot 			= slot
	self.backpackType 	= type
	
	-- Update the item count
	self:UpdateItemCount()
	
	-- Update the icon display
	DynamicImageSetTexture( self.button.name .. "Icon", texture, dx, dy )
	WindowSetTintColor( self.button.overlay.name, color.r, color.g, color.b)
end

function Interface:UpdateItem( itemData, slot, type )
	-- Sanity check the data
	if( itemData ~= nil and slot ~= nil and type ~= nil ) then
		-- If we do not have an item, go ahead and set it
		if( self.itemData ~= nil ) then
			-- If we do have an item, check to make sure its a new item before proceeding
			if( self.itemData.uniqieID ~= itemData.uniqueID or self.slot ~= slot or self.backpackType ~= type ) then
				self:SetItemDataExplicit( itemData, slot, type )
			end
		else
			self:SetItemDataExplicit( itemData, slot, type )	
		end
	else
		self:SetItemDataExplicit( nil, nil, nil )
	end
end

function Interface:RemoveCraftingItem()
	if( self.itemData ~= nil and self.slot ~= nil and self.backpackType ~= nil ) then
		Motion.Debug( "Interface:RemoveCraftingItem" )
		RemoveCraftingItem( Motion.GetPlayerCraftType(), self.slot, self.backpackType )
		self.slotted = false
	end	
end

function Interface:UpdateItemCount()
	local count = 0
	local display = L""
	if( self.itemData ~= nil ) then
		count = self:GetItemCount()
		if( count > 999 ) then
			count = 999
		elseif( count == 0 ) then
			self:SetItemData( nil )
		end
		display = towstring( count )
	end
	LabelSetText( self.button.name .. "Count", display )
end
